

class NameClass:

    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def rename(self, new_name):
        print("Renaming!\n")
        self.name = new_name
        return
